import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AddBlog from '../screens/Blog/AddBlog';
import BlogDetail from '../screens/Blog/BlogDetail';
import Blogs from '../screens/Blog/Blogs';
import HomePage from '../screens/HomePage';
import CheckHotel from '../screens/Hotel/CheckHotel';
import HotelDetail from '../screens/Hotel/HotelDetail';
import HotelsList from '../screens/Hotel/HotelsList';
import { ROUTE_CONSTANT_VARIABLE } from '../utils/constant';

/** Route constant with component mapping */
export const routes = [
  {
    path: ROUTE_CONSTANT_VARIABLE.HOME,
    component: HomePage,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.BLOGS,
    component: Blogs,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.ADD_BLOG,
    component: AddBlog,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.BLOG_DETAIL,
    component: BlogDetail,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.HOTEL,
    component: CheckHotel,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.HOTELS_LIST,
    component: HotelsList,
  },
  {
    path: ROUTE_CONSTANT_VARIABLE.HOTEL_DETAIL,
    component: HotelDetail,
  },
];

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        {routes?.map(({ path, component }) => (
          <Route key={path} path={path} component={component} exact />
        ))}
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
