/** Constants here */

/** URL Constants */
export const URL = {
  BASE_URL_BLOG: process.env.REACT_APP_BASE_URL,
  BASE_URL_HOTEL: process.env.REACT_APP_BASE_URL,
};

/** Route constants */
export const ROUTE_CONSTANT_VARIABLE = {
  HOME: '/',
  BLOGS: '/blogs',
  ADD_BLOG: '/blog/add',
  BLOG_DETAIL: '/blog/detail/:blogId',
  HOTEL: '/hotel',
  HOTELS_LIST: '/hotels/:arrival&:departure',
  HOTEL_DETAIL: '/hotels/detail/:arrival&:departure/:hotelId',
};
