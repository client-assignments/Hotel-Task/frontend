import React from 'react';
import { useHistory } from 'react-router-dom';
import { ROUTE_CONSTANT_VARIABLE } from '../utils/constant';

/** This component will help user to navigate to bolg and hotel route */
function HomePage() {
  const history = useHistory();
  return (
    <div className="homePageWrapper">
      <div onClick={() => history.push(ROUTE_CONSTANT_VARIABLE.BLOGS)}>Blog Demo</div>
      <div onClick={() => history.push(ROUTE_CONSTANT_VARIABLE.HOTEL)}>Hotel Demo</div>
    </div>
  );
}

export default HomePage;
