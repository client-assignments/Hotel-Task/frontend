import { Link, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import { useParams } from 'react-router';
import Loader from '../../components/Blog/Loader';
import { URL } from '../../utils/constant';
import Header from '../../components/Blog/Header';

const useStyle = makeStyles(() => ({
  paperContainer: {
    margin: '5% 20%',
    padding: '50px 20px',
    backgroundColor: '#e2f3f5',
  },
  '@media (max-width: 960px)': {
    paperContainer: { padding: '20px', margin: '10%' },
  },
  userInfo: {
    display: 'flex',
  },
  header: {
    textAlign: 'left',
    fontSize: '28px',
    fontWeight: 700,
    margin: '40px 0 20px 0',
    color: '#303a52',
  },
  userInfoTab: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '0 0 0 15px',
  },
  userInfoCreatedBy: {
    textAlign: 'left',
    color: '#747474',
  },
  avatar: {
    height: '70px',
    width: '70px',
    borderRadius: '50%',
  },
  placeDetail: {
    fontSize: 35,
    fontWeight: 600,
  },
  activityList: {
    display: 'flex',
    flexDirection: 'column',
  },
  activityListItem: {
    fontSize: 20,
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    gap: '10px',
  },
  activityListHeader: {
    fontSize: '24px',
    fontWeight: 600,
    color: '#364f6b',
  },
  ImageContainer: {
    display: 'grid',
  },
  ImageListItem: {
    width: '200px',
  },
  imageContainerHeader: {
    fontSize: '24px',
    fontWeight: 600,
    color: '#364f6b',
  },
  descriptionContainer: {
    textAlign: 'justify',
    fontSize: 20,
    display: 'flex',
    flexDirection: 'column',
    gap: '40px',
  },
  description: {
    textIndent: '50px',
    color: '#364f6b',
    textAlign: 'justify',
  },
  placeTab: {},
  paperContainerEmpty: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    height: 'calc(100vh - 40px)',
  },
  paperContainerEmptyTitle: {
    fontSize: 35,
    fontWeight: '600',
  },
  subDetailContainer: {
    padding: '40px 15px',
    display: 'flex',
    flexDirection: 'column',
    gap: '50px',
    backgroundColor: '#f5f5f5',
    borderRadius: '5px',
  },
}));

function BlogDetail() {
  const classes = useStyle();
  const { blogId: id } = useParams();
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  /** this function will fetch specific blog data by passing blog id and updates the data state by fetched data */
  const fetchData = async () => {
    try {
      setLoading(true);
      const response = await fetch(`${URL.BASE_URL_BLOG}/api/blog/${id}`, {
        headers: { 'ngrok-skip-browser-warning': 'xyz' },
      });
      const res = await response.json();
      setData(res);
    } catch (e) {
      // Error handling
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <Header headerTitle="Blog Detail" />
      {loading ? (
        <Loader />
      ) : (
        <div>
          {data ? (
            <Paper className={classes.paperContainer}>
              <div className={classes.detailContainer}>
                <div className={classes.subDetailContainer}>
                  <div className={classes.userInfo}>
                    <div className={classes.userInfoAvatar}>
                      <img
                        src="https://demo.ghost.io/content/images/size/w100/2018/10/mlk.jpg"
                        alt="avatar"
                        className={classes.avatar}
                      />
                    </div>
                    <div className={classes.userInfoTab}>
                      <span className={classes.userInfoCreatedBy}>Created By</span>
                      <span className={classes.userInfoName}>
                        {data?.firstname ? data?.firstname : '-'}
                        &nbsp;
                        {data?.lastname ? data?.lastname : '-'}
                        &nbsp;
                        <span className={classes.userInfoCreatedBy}>From</span>
                        &nbsp; &nbsp;
                        {data?.country ? data?.country : '-'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className={classes.descriptionContainer}>
                  <div className={classes.placeTab}>
                    <div className={classes.header}>{data?.blogheader ? data?.blogheader : 'nothing to show here'}</div>
                    <div className={classes.description}>{data?.description}</div>
                  </div>
                  <div className={classes.activityList}>
                    <div className={classes.activityListHeader}>
                      <span>Activity list</span>
                    </div>
                    <div className={classes.activityListItem}>
                      <ul style={{ margin: '10px 0 0 0' }}>
                        {data?.activity?.map((res, index) => (
                          <li>
                            <Link underline="none" sx={{ fontSize: 18, color: '#2f89fc' }} href={res.url} key={index}>
                              <span>{res.place}</span>
                            </Link>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                  <div className={classes.imageContainerHeader}>Photo Gallery</div>
                  <div className={classes.ImageContainer}>
                    <Box
                      sx={{
                        display: 'grid',
                        gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))',
                        rowGap: '15px',
                        columnGap: '20px',
                        padding: '30px',
                      }}
                    >
                      {data?.image?.length ? (
                        data?.image.map((res) => (
                          <img
                            className={classes.ImageListItem}
                            sizes="(max-width: 1000px) 200px, 400px"
                            src={`${URL.BASE_URL_BLOG}/${res}`}
                            alt="Out to Sea"
                            loading="lazy"
                          />
                        ))
                      ) : (
                        // dummy image while no image data received
                        <div>
                          <img
                            className={classes.ImageListItem}
                            src="https://orstx.org/wp-content/uploads/2019/10/no-photo-available-icon-12.jpg"
                            alt="Out to Sea"
                            loading="lazy"
                          />
                        </div>
                      )}
                    </Box>
                  </div>
                </div>
              </div>
            </Paper>
          ) : (
            <Paper className={classes.paperContainerEmpty}>
              <div className={classes.paperContainerEmptyTitle}>Nothing to show here</div>
            </Paper>
          )}
        </div>
      )}
    </div>
  );
}

export default BlogDetail;
