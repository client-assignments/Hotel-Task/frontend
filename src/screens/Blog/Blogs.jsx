import * as React from 'react';
import Box from '@mui/material/Box';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import Loader from '../../components/Blog/Loader';
import Header from '../../components/Blog/Header';
import { URL } from '../../utils/constant';

/** Mui component styles */
const useStyles = makeStyles(() => ({
  headerTitle: {
    textAlign: 'left',
  },
  list: {
    display: 'flex',
    flexDirection: 'row',
    width: ' 550px',
    height: '80px',
    backgroundColor: '#f5f5f5',
    padding: '20px',
    borderRadius: 15,
    textDecoration: 'none',
    color: '#303a52',
  },
  number: {
    height: ' 80px',
    width: ' 80px',
    display: ' flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '40px',
    borderRight: 'solid',
  },
  informationPlace: {
    textOverflow: 'ellipsis',
    whiteSpace: 'inherit',
    overflow: 'hidden',
    display: '-webkit-box',
    '-webkit-line-clamp': 1,
    '-webkit-box-orient': 'vertical',
    fontSize: '20px',
  },
  information: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    gap: '10px',
    paddingLeft: '25px',
  },
  listContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '50px',
    gap: '20px',
  },
  informationName: {
    fontSize: 17,
    textAlign: 'left',
    color: '#8a86a8',
  },
  informationPlaceCountry: {
    color: '#5039ff',
  },
  informationAndIcon: {
    alignItems: 'center',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  arrowIconContainer: {
    paddingRight: '20px',
  },
  arrowIcon: {
    height: 50,
    width: 50,
  },
}));
function Blogs() {
  const [loading, setLoading] = useState(false);
  const [list, setList] = useState([]);
  const classes = useStyles();

  /** This function will fetch available blogs and state with blog data */
  const fetchList = async () => {
    try {
      setLoading(true);
      const response = await fetch(`${URL.BASE_URL_BLOG}/api/blog/list`, {
        headers: { 'ngrok-skip-browser-warning': 'xyz' },
      });
      const listOfData = await response.json();
      setList(listOfData);
    } catch (e) {
      /** error occured while fetching blogs */
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchList();
  }, []);

  return (
    <div>
      <Header headerTitle="Home Page" showAddIcon />
      {loading ? (
        /** If fetching then loader will be displayed o/w list of blogs */
        <Loader />
      ) : (
        <div>
          {list?.length ? (
            <Box className={classes.listContainer}>
              {list?.map((res, index) => (
                /** Mapping through list of blogs and creating card to display data */
                <Link className={classes.list} to={`/blog/detail/${res._id}`}>
                  <div className={classes.number}>{index + 1}</div>
                  <div className={classes.informationAndIcon}>
                    <div className={classes.information}>
                      <div className={classes.informationPlace}>{res.blogheader}</div>
                      <div className={classes.informationName}>
                        {`${res.firstname} 
                      ${res.lastname} From `}
                        <span className={classes.informationPlaceCountry}>{res.country}</span>
                      </div>
                    </div>
                    <div className={classes.arrowIconContainer}>
                      <ArrowRightAltIcon className={classes.arrowIcon} />
                    </div>
                  </div>
                </Link>
              ))}
            </Box>
          ) : (
            /** If no blogs available */
            <center>
              <h2 style={{ color: '#303a52' }}>No Blogs Available</h2>
            </center>
          )}
        </div>
      )}
    </div>
  );
}
export default Blogs;
