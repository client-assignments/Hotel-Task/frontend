import React, { useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardContent } from '@mui/material';
import ApartmentOutlinedIcon from '@mui/icons-material/ApartmentOutlined';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import { useHistory, useParams } from 'react-router';
import CircularProgress from '@mui/material/CircularProgress';
import { URL } from '../../utils/constant';

const useStyles = makeStyles({
  header: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 58,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '10px 100px',
    top: 0,
    left: 0,
    right: 0,
    position: 'sticky',
  },
  listContainer: {
    display: 'grid',
    gridTemplateColumns: 'repeat(2 , 1fr)',
    gap: '20px',
    padding: '40px 30px',
  },
  cardContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  detailContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    gap: '40px',
  },
  addressField: {
    display: 'flex',
    alignItems: 'center',
    gap: '3px',
  },
  loader: {
    marginTop: '50px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  '@media (max-width: 1024px)': {
    detailContainer: {
      gap: '20px',
    },
  },
  '@media (max-width: 820px)': {
    listContainer: {
      gridTemplateColumns: 'repeat(1 , 1fr)',
    },
    detailContainer: {
      gap: '20px',
    },
    header: {
      fontSize: '15px',
    },
  },
});

/** fetch and displays hotel list */
function HotelList() {
  const classes = useStyles();
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  // Reading dates from url params
  const { arrival: checkIn, departure: checkOut } = useParams();
  const [hotelData, setHotelData] = useState([]);

  /** This function will fetch the hotel list data and updates hotel data state */
  const fetchHotelList = useCallback(async () => {
    setIsLoading(true);

    try {
      const response = await fetch(
        `${URL.BASE_URL_HOTEL}/api/properties/list?arrivalDate=${checkIn}&departureDate=${checkOut}`,
      );

      if (!response.ok) {
        throw new Error('Something went Wrong !');
      }
      const data = await response.json();
      setHotelData(data.data);
    } catch (e) {
      // Error handling
    }
    setIsLoading(false);
  }, []);

  /** if date passed then n only calls fetch function */
  useEffect(() => {
    if (checkIn && checkOut) {
      fetchHotelList();
    }
  }, []);

  return (
    <div>
      <header className={classes.header}>
        <h1>Available Hotels</h1>
      </header>

      {isLoading ? (
        <div className={classes.loader}>
          <CircularProgress />
        </div>
      ) : (
        <div className={classes.listContainer}>
          {/* Hotel cards */}
          {hotelData?.map((hotelDetail) => (
            <Card className={classes.card} key={hotelDetail.hotelId}>
              <CardContent className={classes.cardContainer}>
                <div className={classes.detailContainer}>
                  <ApartmentOutlinedIcon sx={{ fontSize: 80 }} />
                  <div>
                    <h2>{hotelDetail?.hotelName}</h2>
                    <div className={classes.addressField}>
                      <LocationOnIcon />
                      {hotelDetail?.address}
                    </div>
                  </div>
                </div>
                <div className={classes.detailContainer}>
                  <div>
                    <p>{hotelDetail?.countryTrans}</p>
                    <p>{hotelDetail?.currencyCode}</p>
                  </div>
                  <div
                    onClick={() =>
                      history.push(
                        `/hotels/detail/${hotelDetail.arrivalDate}&${hotelDetail.departureDate}/${hotelDetail.hotelId}`,
                      )
                    }
                    style={{ cursor: 'pointer' }}
                  >
                    <ArrowRightAltIcon sx={{ fontSize: 50 }} />
                  </div>
                </div>
              </CardContent>
            </Card>
          ))}
        </div>
      )}
    </div>
  );
}

export default HotelList;
