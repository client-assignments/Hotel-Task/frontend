import React, { useCallback, useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardContent, Chip } from '@mui/material';
import { useParams } from 'react-router';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CircularProgress from '@mui/material/CircularProgress';
import { URL } from '../../utils/constant';

const useStyles = makeStyles({
  header: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 58,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '10px 100px',
  },
  card: {
    width: '90%',
    margin: '30px auto',
  },
  detailContainer: {
    width: '100%',
    display: 'grid',
    gridTemplateColumns: '3fr 1fr',
    gap: '40px',
  },
  addressField: {
    display: 'flex',
    alignItems: 'center',
    gap: '3px',
    fontWeight: 'bold',
  },
  address: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    gap: '3px',
  },
  cardsContainer: {
    display: 'grid',
    gridTemplateColumns: 'repeat(3 , 1fr)',
    gap: '10px',
  },

  facilitiesContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: '3px',
  },
  roomPhoto: {
    width: '150px',
    height: '150px',
  },
  photosContainer: {
    display: 'flex',
    flexDirection: 'row',
    gap: '16px',
    overflowX: 'auto',
    paddingBottom: '10px',
  },
  loader: {
    marginTop: '50px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

/** This component fetch and displays the specific hotel data  */
function HotelDetail() {
  const classes = useStyles();
  const { arrival: checkIn, departure: checkOut, hotelId } = useParams();
  const [isLoading, setIsLoading] = useState(false);
  const [hotelDetails, setHotelDetails] = useState({});
  const [roomData, setRoomData] = useState([]);

  /** this function will fetch the hotel data and updates hotel and room detail state */
  const fetchHotelDetail = useCallback(async () => {
    setIsLoading(true);

    try {
      const response = await fetch(
        `${URL.BASE_URL_HOTEL}/api/properties/details?arrivalDate=${checkIn}&departureDate=${checkOut}&hotelId=${hotelId}`,
      );

      if (!response.ok) {
        throw new Error('Something went Wrong !');
      }
      const data = await response.json();
      setHotelDetails(data.data);
      setRoomData(Object.values(data.data.roomsImageAndFacility));
    } catch (e) {
      // error handling
    }
    setIsLoading(false);
  }, []);

  /** if dates and hotel id were in URL param then n only calls fetch function */
  useEffect(() => {
    if (checkIn && checkOut && hotelId) {
      fetchHotelDetail();
    }
  }, []);

  return (
    <>
      <header className={classes.header}>
        <h1>Details</h1>
      </header>

      {isLoading ? (
        <div className={classes.loader}>
          <CircularProgress />
        </div>
      ) : (
        // hotel detail card
        <Card className={classes.card}>
          <CardContent className={classes.cardContent}>
            <div className={classes.detailContainer}>
              <div>
                <h1>{hotelDetails?.hotelName}</h1>
                <div>rating</div>
                <p>{hotelDetails?.description}</p>
              </div>

              <div className={classes.address}>
                <div className={classes.addressField}>
                  <LocationOnIcon />
                  <p>{hotelDetails?.address}</p>
                </div>
                <div>{hotelDetails.countryTrans}</div>
                <div>{hotelDetails.currencyCode}</div>
              </div>
            </div>
            <h3>Available Rooms</h3>
            <div className={classes.cardsContainer}>
              {/* Room cards with facility and images */}
              {roomData.map((roomDetail) => (
                <Card className={classes.innerCard}>
                  <CardContent>
                    <div>
                      <h4>Facilities</h4>
                      <div className={classes.facilitiesContainer}>
                        {roomDetail.facilities.map((facility) => (
                          <Chip label={facility} />
                        ))}
                      </div>
                    </div>

                    <div className={classes.photosWrapper}>
                      <h4>Photos</h4>
                      <div className={classes.photosContainer}>
                        {roomDetail.photos.map((photoURL) => (
                          <img src={photoURL} className={classes.roomPhoto} />
                        ))}
                      </div>
                    </div>
                  </CardContent>
                </Card>
              ))}
            </div>
          </CardContent>
        </Card>
      )}
    </>
  );
}

export default HotelDetail;
