import { Button, Card, CardContent, CardHeader, TextField, Icon, InputAdornment } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { useState } from 'react';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { useHistory } from 'react-router';
import { format } from 'date-fns';

const useStyles = makeStyles({
  header: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 58,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '10px 100px',
  },
  card: { width: '60%', margin: '60px auto', padding: '30px' },
  cardContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: '10px',
  },
  searchButton: { height: '50px' },
  // mobile support
  '@media (max-width: 1024px)': { card: { width: '70%' }, cardContainer: { gap: '20px' } },
  '@media (max-width: 820px)': {
    card: { width: '80%' },
    cardContainer: { flexWrap: 'wrap', gap: '20px' },
    header: { fontSize: '15px' },
  },
});

/** To take date range from user */
function CheckHotel() {
  const classes = useStyles();
  const [openDatePicker, setOpenDatePicker] = useState(false);
  const [openCheckoutDatePicker, setOpenCheckoutDatePicker] = useState(false);
  const [checkInDate, setCheckInDate] = useState(format(new Date(), 'yyyy-MM-dd'));
  const [checkOutDate, setCheckOutDate] = useState(format(new Date(), 'yyyy-MM-dd'));
  const history = useHistory();
  const [errorForCheckOut, setErrorForCheckOut] = useState(null);
  const [errorForCheckIn, setErrorForCheckIn] = useState(null);

  /** manages state on data chnages */
  const onChangeDetail = (name, value) => {
    if (name === 'checkInDate') {
      setCheckInDate(value);
      setErrorForCheckIn(null);
    } else if (name === 'checkOutDate') {
      setCheckOutDate(value);
      if (Date.parse(checkInDate) > Date.parse(value)) {
        setErrorForCheckOut('Please Select Check-out Date greater then check-in');
      } else {
        setErrorForCheckOut(null);
      }
    }
  };

  // This function will manage validations and redirects users to hotel list with passed dates
  const clickHandler = () => {
    if (checkInDate.length === 0 && checkOutDate.length === 0) {
      setErrorForCheckIn('Please set Arrival Date');
      setErrorForCheckOut('Please set Departure Date');
    } else if (checkInDate.length === 0) {
      setErrorForCheckOut(null);
      setErrorForCheckIn('Please set Arrival Date');
    } else if (checkOutDate.length === 0) {
      setErrorForCheckIn(null);
      setErrorForCheckOut('Please set Departure Date');
    } else if (new Date(checkInDate).getTime() >= new Date(checkOutDate).getTime()) {
      setErrorForCheckIn(null);
      setErrorForCheckOut('Please Select Departure Date greater then Arrival Date');
    } else {
      const arrivalDate = format(new Date(checkInDate), 'yyyy-MM-dd');
      const departureDate = format(new Date(checkOutDate), 'yyyy-MM-dd');
      if (arrivalDate && departureDate) {
        history.push(`/hotels/${arrivalDate}&${departureDate}`);
      }
    }
  };

  return (
    <>
      <header className={classes.header}>
        <h1>Search Hotel</h1>
      </header>
      <Card className={classes.card}>
        <CardHeader title="Find Hotels" />
        <CardContent className={classes.cardContainer}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            {/* MUIX date pickers */}
            <DatePicker
              key="check-in"
              label="Select Arrival Date"
              value={checkInDate || new Date()}
              open={openDatePicker}
              onClose={() => setOpenDatePicker(false)}
              openTo="day"
              inputFormat="dd MMMM yyyy"
              onChange={(e) => {
                setOpenDatePicker(true);
                onChangeDetail('checkInDate', e);
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon>
                      <CalendarMonthIcon />
                    </Icon>
                  </InputAdornment>
                ),
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={!!errorForCheckIn}
                  helperText={errorForCheckIn}
                  onClick={() => setOpenDatePicker(true)}
                  inputProps={{ ...params.inputProps, placeholder: 'Select Date', readOnly: true }}
                />
              )}
            />
          </LocalizationProvider>

          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              key="check-out"
              label="Select Departure Date"
              value={checkOutDate || new Date()}
              open={openCheckoutDatePicker}
              onClose={() => setOpenCheckoutDatePicker(false)}
              openTo="day"
              inputFormat="dd MMMM yyyy"
              onChange={(e) => {
                setOpenCheckoutDatePicker(true);
                onChangeDetail('checkOutDate', e);
              }}
              onError={() => {}}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon>
                      <CalendarMonthIcon />
                    </Icon>
                  </InputAdornment>
                ),
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={!!errorForCheckOut}
                  helperText={errorForCheckOut}
                  onClick={() => setOpenCheckoutDatePicker(true)}
                  inputProps={{ ...params.inputProps, placeholder: 'Select Date', readOnly: true }}
                />
              )}
            />
          </LocalizationProvider>

          <Button variant="contained" className={classes.searchButton} onClick={clickHandler}>
            Search
            <ChevronRightIcon />
          </Button>
        </CardContent>
      </Card>
    </>
  );
}

export default CheckHotel;
