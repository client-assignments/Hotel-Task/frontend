import { makeStyles } from '@mui/styles';
import AddCardIcon from '@mui/icons-material/AddCard';
import { Link } from 'react-router-dom';
import { ROUTE_CONSTANT_VARIABLE } from '../../utils/constant';

const useStyle = makeStyles(() => ({
  headerContainer: {
    height: '80px',
    padding: '0 100px',
    backgroundColor: 'rgba(0,122,255,0.99)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontSize: '24px',
    color: '#fff',
    fontWeight: 600,
  },
}));

/** Common header component for blog task */
function Header({ headerTitle, showAddIcon }) {
  const classes = useStyle();
  return (
    <div className={classes.headerContainer}>
      <div>{headerTitle}</div>
      {/* if passed then shows icon on right side */}
      {showAddIcon && (
        <Link to={ROUTE_CONSTANT_VARIABLE.ADD_BLOG}>
          <AddCardIcon style={{ color: '#fff' }}>Add</AddCardIcon>
        </Link>
      )}
    </div>
  );
}
export default Header;
