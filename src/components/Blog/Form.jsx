import React, { useState } from 'react';
import { Button, TextField, IconButton, Snackbar, Alert } from '@mui/material';
import { makeStyles } from '@mui/styles';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import { useHistory } from 'react-router-dom';
import DropZoneComponent from './FileUpload';
import Header from './Header';
import { ROUTE_CONSTANT_VARIABLE, URL } from '../../utils/constant';

const useStyle = makeStyles(() => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    padding: '50px',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    gap: '30px',
    width: 500,
  },
  activityWrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: '15px',
  },
  ActivityGroup: {
    display: 'flex',
    gap: 10,
  },
  addActivityBar: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: '600',
  },
  activityTitle: {
    display: 'flex',
    width: '80%',
    alignItems: 'center',
  },
  buttonWrapper: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    gap: '20px',
  },
  button: {
    height: 50,
    textAlign: 'center',
  },
}));

function Form() {
  const [files, setFiles] = useState([]);
  /** Initial blog data */
  const [blogData, setBlogData] = useState({
    firstName: '',
    lastName: '',
    countryName: '',
    blogHeader: '',
    description: '',
  });
  const [errorData, setErrorData] = useState({});
  const [activity, setActivity] = useState([]);
  const classes = useStyle();
  const history = useHistory();
  const [isError, setError] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  /** this function will manage field changes to blogdata object dynamicly */
  const handleFieldChange = (data) => {
    setBlogData((e) => ({
      ...e,
      [data.target.name]: data.target.value,
    }));
  };

  /** this function will push new activity to activity array  */
  const addActivity = () => {
    setActivity((e) => [
      ...e,
      {
        place: '',
        url: '',
      },
    ]);
  };

  /** This function will remove activity from activity array */
  const removeActivity = (index) => {
    setActivity([...[...activity].splice(0, index), ...[...activity].splice(index + 1, activity.length - 1)]);
  };

  /** this function will manage activity field changes dynamicly to activity state */
  const handleActivityChange = (e, index) => {
    const activities = [...activity];
    activities[index] = {
      ...activities[index],
      [e.target.name]: e.target.value,
    };
    setActivity(activities);
  };

  /** this function will return do validations and return true if validation passed otherwise set an error state and return false */
  const validate = (data) => {
    const error = {};
    if (!data.firstName) {
      error.firstName = ' First name is required';
    }
    if (!data.lastName) {
      error.lastName = 'last name is required';
    }
    if (!data.countryName) {
      error.countryName = 'country name is required';
    }
    if (!data.blogHeader) {
      error.blogHeader = 'blogHeader is required';
    }
    if (!data.description) {
      error.description = 'Description is required';
    }
    if (Object.keys(error).length === 0) {
      setErrorData(null);
      return true;
    }
    setErrorData(error);
    return false;
  };

  /** redirects user to home page */
  const handleCancel = () => {
    history.push(ROUTE_CONSTANT_VARIABLE.BLOGS);
  };

  /** this function will send filled blog data to backend */
  const sendData = async (data) => {
    try {
      const res = await fetch(`${URL.BASE_URL_BLOG}/api/addblog`, {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'ngrok-skip-browser-warning': 'sadfsdfsdfsd',
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      });
      if (res) {
        setIsSuccess(true);
        setTimeout(() => {
          history.push(ROUTE_CONSTANT_VARIABLE.BLOGS);
        }, 6000);
      }
    } catch (e) {
      setError(true);
    } finally {
      /** */
    }
  };

  /** this function will convert image to base64 data */
  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  /** this function will manage convertion of image to base64 for multiple files */
  const getImages = async () => {
    const results = await Promise.all(
      files.map(async (item) => {
        const base64 = await toBase64(item);
        return base64;
      }),
    );
    return results;
  };

  /** this function will do validations and sumbmittion of blog data */
  const handelSubmit = async (e) => {
    e.preventDefault();
    const isValidated = validate(blogData);
    if (isValidated) {
      sendData({
        firstname: blogData.firstName,
        lastname: blogData.lastName,
        country: blogData.countryName,
        blogheader: blogData.blogHeader,
        description: blogData.description,
        activity,
        images: await getImages(),
      });
    }
  };

  return (
    <>
      <Header headerTitle="Add Blog" showAddIcon={false} />
      <div className={classes.container}>
        <form onSubmit={handelSubmit} className={classes.form}>
          <TextField
            fullWidth
            id="firstName"
            name="firstName"
            label="First Name"
            value={blogData.firstName}
            onChange={handleFieldChange}
            error={errorData?.firstName}
            helperText={errorData?.firstName}
          />
          <TextField
            fullWidth
            id="lastName"
            name="lastName"
            label="Last Name"
            value={blogData.lastName}
            onChange={handleFieldChange}
            error={errorData?.lastName}
            helperText={errorData?.lastName}
          />
          <TextField
            fullWidth
            id="countryName"
            name="countryName"
            label="Country Name"
            value={blogData.countryName}
            onChange={handleFieldChange}
            error={errorData?.countryName}
            helperText={errorData?.countryName}
          />
          <TextField
            fullWidth
            id="blogHeader"
            name="blogHeader"
            label="Blog Header"
            value={blogData.blogHeader}
            onChange={handleFieldChange}
            error={errorData?.blogHeader}
            helperText={errorData?.blogHeader}
          />
          <TextField
            fullWidth
            id="description"
            name="description"
            label="Description"
            value={blogData.description}
            multiline
            rows={5}
            onChange={handleFieldChange}
            error={errorData?.description}
            helperText={errorData?.description}
          />
          <div className={classes.activityWrapper}>
            <div className={classes.addActivityBar}>
              <div className={classes.activityTitle}>Add activity</div>
              <IconButton aria-label="add" color="primary" onClick={addActivity}>
                <AddIcon />
              </IconButton>
            </div>
            {activity.map((e, index) => (
              <React.Fragment key={index}>
                <div className={classes.ActivityGroup}>
                  <TextField
                    fullWidth
                    id="activity"
                    name="place"
                    label="place"
                    value={e?.name}
                    onChange={(data) => handleActivityChange(data, index)}
                  />
                  <TextField
                    fullWidth
                    id="activity"
                    name="url"
                    label="Link of activity"
                    value={e?.url}
                    onChange={(data) => handleActivityChange(data, index)}
                  />

                  <IconButton onClick={() => removeActivity(index)}>
                    <DeleteIcon />
                  </IconButton>
                </div>
              </React.Fragment>
            ))}
          </div>
          {/* handles file upload */}
          <DropZoneComponent files={files} setFiles={setFiles} />
          <div className={classes.buttonWrapper}>
            <Button
              color="error"
              variant="contained"
              fullWidth
              type="cancel"
              className={classes.button}
              onClick={handleCancel}
            >
              Cancel
            </Button>
            <Button
              color="primary"
              variant="contained"
              fullWidth
              type="submit"
              disabled={isSuccess}
              className={classes.button}
            >
              Submit
            </Button>
          </div>
        </form>
      </div>
      {/* Handles notification for success and failure */}
      <Snackbar
        open={isError || isSuccess}
        autoHideDuration={5000}
        onClose={() => {
          setIsSuccess(false);
          setError(false);
        }}
      >
        <Alert
          onClose={() => {
            setIsSuccess(false);
            setError(false);
          }}
          severity={isSuccess ? 'success' : 'error'}
          sx={{ width: '100%' }}
        >
          {isSuccess ? 'Blog added successfully!' : 'Failed to add blog!'}
        </Alert>
      </Snackbar>
    </>
  );
}

export default Form;
