import React from 'react';
import { CircularProgress } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyle = makeStyles(() => ({
  loader: {
    display: 'flex',
    width: '100vw',
    height: '100vh',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
}));
// Common loader for blog task
function Loader() {
  const classes = useStyle();
  return (
    <div className={classes.loader}>
      <CircularProgress />
    </div>
  );
}

export default Loader;
