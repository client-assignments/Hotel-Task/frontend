# Demo Task

### Task 
1. Blog task demo
2. Hotel task demo

### Introduction:
A react project with two different task code one for blog system task and one for hotel task

- Blog task demo:

  - Displays list of available blogs.
  - Allows user to add new blog.
  - Display specific blog in detail.

- Hotel task demo:
  - User can search available hotels by passing valide date renage.
  - Shows the list of available hotels properties by filtering passed dates.
  - Displays in detail hotel property with available list of rooms and facilities.

## Installation & Configuration

- Clone the repository on your local machine. After cloning the project, follow these steps:

1. Install dependencies via yarn.

```bash
yarn install # (for local setup)
```
2. Create an .env file with valid values that is identical to .env

3. To start the server

```bash
yarn start
```

## .env Reference

In .env file have to set URL variables.
- REACT_APP_BASE_URL : This variables stores the backend endpoint.
- Example. 
```bash 
REACT_APP_BASE_URL=https://localhost:3001
```

### Folder Structure

Common structure that is used in this project is as following
```
.
└── src
    └── components
        └── component.jsx

    └── routes
        └── route.js

    └── screens
        └── UI screens

    └── utils
        └── constant
        └── helper
```